package com.oop.orangeengine.hologram;

public interface HologramAttachable {

    void onAttach(Hologram hologram);

}
